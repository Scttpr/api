# Mobility Recorder

![GitHub package.json version](https://img.shields.io/github/package-json/v/Mobility-Recorder/api?style=for-the-badge)
![GitHub](https://img.shields.io/github/license/Mobility-Recorder/api?style=for-the-badge)
![GitHub top language](https://img.shields.io/github/languages/top/Mobility-Recorder/api?style=for-the-badge)
![GitHub last commit](https://img.shields.io/github/last-commit/Mobility-Recorder/api?style=for-the-badge)

## Description

PoC API with Nest.js before a Rust implementation

## Installation

```bash
$ npm install
$ cp config.dist.json config.json
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Stay in touch

- Author - [Scttpr](https://github.com/Scttpr)

## License

  Mobility Recorder is [GNU General Public License v3.0 licensed](./LICENSE).
