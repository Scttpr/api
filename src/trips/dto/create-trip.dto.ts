import {
    IsDate,
    IsEnum,
    IsString,
    Max,
    IsNumber,
    ValidateIf,
    IsBoolean,
    Length,
    ValidateNested, Min,
} from 'class-validator';
import { ApiProperty } from "@nestjs/swagger";

import { BicycleSubMode, Mode, MainMode, Motive, PublicTransportSubMode, Scope, Weather } from './trips';

export class CreateTripDto {
    @ApiProperty({ example: '18 rue de Paris, 75000 Paris', description: 'Trip origin address' })
    @IsString()
    @Max(50)
    readonly origin: string;

    @ApiProperty({ example: '45 rue de Paris, 75000 Paris', description: 'Trip destination address' })
    @IsString()
    @Max(50)
    readonly destination: string;

    @ApiProperty({ description: 'Trip start date' })
    @IsDate()
    readonly date: Date;

    @ApiProperty({ example: { mode: MainMode.PUBLIC_TRANSPORT, submode: PublicTransportSubMode.TRAMWAY }, description: 'Most used mode during trip'})
    @ValidateNested()
    readonly heaviestMode: Mode;

    @ApiProperty({
        example: [
            { mode: MainMode.BICYCLE, submode: BicycleSubMode.PERSONAL_BIKE, ditance: 1.2 },
            { mode: MainMode.PUBLIC_TRANSPORT, submode: PublicTransportSubMode.TRAMWAY, distance: 6.8 },
            { mode: MainMode.BICYCLE, submode: BicycleSubMode.PERSONAL_BIKE, distance: 0.9 }
        ],
        description: 'Ordered list of all nodes during trips, from origin to destination'
    })
    @ValidateNested()
    readonly modes: Array<Mode>;

    @ApiProperty({ description: 'Motive defined according to surveys grids' })
    @ValidateNested()
    readonly motive: Motive;

    @ApiProperty({ example: Scope.LOCAL ,description: 'Wether the trip is local or related to tourism' })
    @IsEnum(Scope)
    readonly scope: Scope;

    @ApiProperty({ example: 1, description: 'Number of people in the vehicle' })
    @IsNumber()
    @Min(1)
    @Max(500)
    readonly fillingRate: number;

    @ApiProperty({ example: true, description: 'Wether or not driver was the driver during the trip'})
    @ValidateIf(({ isDriver }) => isDriver)
    @IsBoolean()
    readonly isDriver: boolean;

    @ApiProperty({ example: Weather.DRY, description: 'Wether or not it was rainy during trip', required: false })
    @ValidateIf(({ weather }) => weather)
    @IsEnum(Weather)
    readonly weather?: Weather;

    @ApiProperty({ example: 'Some dummy comments', description: 'Comments about the trip', required: false })
    @ValidateIf(({ weather }) => weather)
    @IsString()
    @Length(1, 100)
    readonly comments?: string;
}
