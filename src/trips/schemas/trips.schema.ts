
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class TripDocument extends Document {
    @Prop({ required: true })
    origin: string;

    @Prop({ required: true })
    destination: string;

    @Prop({ required: true })
    date: Date;

    @Prop({ required: true })
    heaviestMode: Document

    @Prop({ required: true })
    modes: [Document]

    @Prop({ required: true })
    motive: Document

    @Prop({ required: true })
    scope: string;

    // @todo check default values for fillingRate
    @Prop({ default: 1 })
    fillingRate: number;

    @Prop({ required: true })
    isDriver: boolean;

    // @todo add last fields later
}

export const TripSchema = SchemaFactory.createForClass(TripDocument);
